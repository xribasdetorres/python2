import pygame



class Bullet(Sprite):

    def __init__(self, settings, screen, ship):

        super(Bullet, self).__init__()
        self.screen = screen;
        self.rect = pygame.Rect(ship.rect.centerx, ship.rect.top, settings.bullet_width, settings.bullet_height)

    def update:
        self.rect.centerx += 1;

    def draw(self):
        pygame.draw.rect(self.screen, (1, 1, 1), self.small_screen_rect);